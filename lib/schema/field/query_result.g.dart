// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'query_result.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FQuerySchema _$FQuerySchemaFromJson(Map<String, dynamic> json) => FQuerySchema(
      documentSchema: json['documentSchema'] as String,
    );

Map<String, dynamic> _$FQuerySchemaToJson(FQuerySchema instance) =>
    <String, dynamic>{
      'documentSchema': instance.documentSchema,
    };
